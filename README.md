# UconMDE

The [Usage Control model (UCON)](https://dl.acm.org/doi/10.1145/984334.984339) is an access control model that was created for complex, dynamic scenarios that demand security policies more complex than those provided by traditional access control models. This repository contains the implementation of a Domain-Specific Language to model UCON security policies and their integration in (model-based) development processes. The implementation has been made on top of the [Eclipse Modeling Framework](https://www.eclipse.org/modeling/emf/), the de-facto standard MDE (Model-Driven Engineering) framework. 

If you would like to know more details about this UCON DSL you could take a look to the following open access publication:

- Antonia M. Reina Quintero, Salvador Martínez Pérez, Ángel Jesús Varela-Vaca, María Teresa Gómez López, Jordi Cabot. *A domain-specific language for the specification of UCON policies*. Journal of Information Security and Applications. Volume 64. 2022. ISSN 2214-2126. https://doi.org/10.1016/j.jisa.2021.103006.

If you use this DSL in your research you could cite our work as follows:

```bibtex
@article{REINAQUINTERO2022103006,
title = {A domain-specific language for the specification of UCON policies},
journal = {Journal of Information Security and Applications},
volume = {64},
pages = {103006},
year = {2022},
issn = {2214-2126},
doi = {https://doi.org/10.1016/j.jisa.2021.103006},
url = {https://www.sciencedirect.com/science/article/pii/S221421262100212X},
author = {Antonia M. {Reina Quintero} and Salvador Martínez Pérez and Ángel Jesús Varela-Vaca and María Teresa Gómez López and Jordi Cabot},
keywords = {Cybersecurity, Access control, Model-driven engineering, UCON, DSL},
abstract = {Security policies constrain the behavior of all users of an information system. In any non-trivial system, these security policies go beyond simple access control rules and must cover more complex and dynamic scenarios while providing, at the same time, a fine-grained level decision-making ability. The Usage Control model (UCON) was created for this purpose but so far integration of UCON in mainstream software engineering processes has been very limited, hampering its usefulness and popularity among the software and information systems communities. In this sense, this paper proposes a Domain-Specific Language to facilitate the modeling of UCON policies and their integration in (model-based) development processes. Together with the language, an exploratory approach for policy evaluation and enforcement of the modeled policies via model transformations has been introduced. These contributions have been defined on top of the Eclipse Modeling Framework, the de-facto standard MDE (Model-Driven Engineering) framework making them freely available and ready-to-use for any software designer interested in using UCON for the definition of security policies in their new development projects.}
}
```

## UCON metamodel

The abstract syntax of our UCON metamodel is depicted graphically as follows. The top level view figure shows the root of the metamodel (`Policy`) as well as the elements needed to describe access-control: `Subject`, `Object`, `Right`, `Rules` and `Environment`. The UCON rules figure shows the rule subset.

Top level view:

![Top Level View](./security.ucon.model/img/TopLevelView.png)

UCON rules:

![UCON Rules](./security.ucon.model/img/UCONRules.png)


